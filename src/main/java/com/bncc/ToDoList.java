package com.bncc;

import java.util.ArrayList;

public class ToDoList {
    private ArrayList<Task> taskList;

    public ToDoList(){
        taskList = new ArrayList<Task>();
    }

    public void addTask(int taskId, String taskName){
        taskList.add(new Task(taskId, taskName));
    }

    public Task getTaskByIndex(int index){
        return taskList.get(index);
    }
    public String getTask(ToDoList toDoList, int index){
        Task task = toDoList.getTaskByIndex(index);
        //String taskStatus = task.getStatus() ? "[DONE]" : "[NOT DONE]";
        String taskString;
        taskString = task.getId()+". "+task.getName()+" ["+task.getStatus()+"]";
        return taskString;
    }

    public String getToDoList(ToDoList toDoList){
        String toDoListString = "";
        for (int i=0;i<taskList.size();i++){
            toDoListString += getTask(toDoList, i);
            toDoListString += "\n";
        }
        return toDoListString;
    }


}
