package com.bncc;

public class Main {
    public static void addTasks(ToDoList toDoList){
        toDoList.addTask(1, "Clean Spoon");
        toDoList.addTask(3, "Do Homework");
        toDoList.addTask(5, "Attend Meetings");
    }

    public static void main(String[] args) {
        ToDoList toDoList = new ToDoList();
        addTasks(toDoList);
        System.out.println("Printing a task");
        System.out.println(toDoList.getTask(toDoList,2));
        System.out.println("Printing to do list");
        System.out.println(toDoList.getToDoList(toDoList));
    }
}
