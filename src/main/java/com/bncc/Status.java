package com.bncc;

public enum Status {
    DONE,
    NOT_DONE;

    @Override
    public String toString() {
        switch(this) {
            case DONE:
                return "DONE";
            case NOT_DONE:
                return "NOT DONE";
            default:
                throw new IllegalArgumentException();
        }
    }
}
