package com.bncc;

public class Task{
    private int id;
    private String name;
    private Status status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Task(int id, String name){
        this.id = id;
        this.name = name;
        this.status = Status.NOT_DONE;
    }
}
