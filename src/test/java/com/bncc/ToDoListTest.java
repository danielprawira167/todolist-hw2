package com.bncc;

import org.junit.Test;

import static org.junit.Assert.*;

public class ToDoListTest {

    @Test
    public void testAddTask() {
    }

    @Test
    public void testGetTaskByIndex() {
        String expectedTask = "1. Clean Spoon [NOT DONE]";
        ToDoList toDoList = new ToDoList();
        toDoList.addTask(1, "Clean Spoon");
        assertEquals(expectedTask, toDoList.getTask(toDoList,0));
    }

    @Test
    public void getTask() {
    }

    @Test
    public void getToDoList() {
        String expectedTask = "1. Clean Spoon [NOT DONE]\n3. Do Homework [NOT DONE]\n5. Attend Meetings [NOT DONE]\n";
        ToDoList toDoList = new ToDoList();
        toDoList.addTask(1, "Clean Spoon");
        toDoList.addTask(3, "Do Homework");
        toDoList.addTask(5, "Attend Meetings");
        assertEquals(expectedTask, toDoList.getToDoList(toDoList));
    }
}